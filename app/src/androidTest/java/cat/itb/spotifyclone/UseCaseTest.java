package cat.itb.spotifyclone;

import androidx.test.ext.junit.rules.ActivityScenarioRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.doubleClick;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class UseCaseTest {

    private String USER_TO_BE_TYPED = "espressoprova12@gmail.com";
    private String PASS_TO_BE_TYPED = "123456789";

    @Rule
    public ActivityScenarioRule<LoginActivity> activityScenarioRule = new ActivityScenarioRule<>(LoginActivity.class);

    @Test
    public void test_logout() {
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.loginbutton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));

        onView(withId(R.id.settingsButton)).perform(click());
        onView(withId(R.id.settingsFragmenttest)).check(matches(isDisplayed()));

        onView(withId(R.id.logutButton)).perform(scrollTo(), click());
    }

    @Test
    public void activate_no_conection_mode() {
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.loginbutton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));
        onView(withId(R.id.settingsButton)).perform(click());
        onView(withId(R.id.settingsFragmenttest)).check(matches(isDisplayed()));

        onView(withId(R.id.switchConection)).perform(scrollTo(), click());
        onView(withId(R.id.switchConection)).check(matches(isChecked()));
    }

    @Test
    public void enter_on_a_album_song() {
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.loginbutton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));

        onView(withId(R.id.recyclerView1)).perform(actionOnItemAtPosition(2,click()));
        onView(withId(R.id.fragmentAlbumList)).check(matches(isDisplayed()));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.titleAlbum)).check(matches(withText("EL ÚLTIMO TOUR DEL MUNDO")));
        onView(withId(R.id.recyclerSongs)).perform(actionOnItemAtPosition(2,click()));
        onView(withId(R.id.playerActivity)).check(matches(isDisplayed()));
    }

    @Test
    public void enter_on_a_album_song_and_pause_player() {
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.loginbutton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));

        onView(withId(R.id.recyclerView1)).perform(actionOnItemAtPosition(2,click()));
        onView(withId(R.id.fragmentAlbumList)).check(matches(isDisplayed()));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.titleAlbum)).check(matches(withText("EL ÚLTIMO TOUR DEL MUNDO")));
        onView(withId(R.id.recyclerSongs)).perform(actionOnItemAtPosition(2,click()));
        onView(withId(R.id.playerActivity)).check(matches(isDisplayed()));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.b_play)).perform(doubleClick());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void enter_on_a_album_song_and_mark_as_favourite() {
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.loginbutton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));

        onView(withId(R.id.recyclerView1)).perform(actionOnItemAtPosition(2,click()));
        onView(withId(R.id.fragmentAlbumList)).check(matches(isDisplayed()));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.titleAlbum)).check(matches(withText("EL ÚLTIMO TOUR DEL MUNDO")));
        onView(withId(R.id.recyclerSongs)).perform(actionOnItemAtPosition(2,click()));
        onView(withId(R.id.playerActivity)).check(matches(isDisplayed()));
        onView(withId(R.id.imageViewCorazon)).perform(click());
    }
}
