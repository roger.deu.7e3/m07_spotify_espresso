package cat.itb.spotifyclone;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class LoginRegisterTest {

    private String USER_TO_BE_TYPED = "espressoprova14@gmail.com";
    private String USERNAME_TO_BE_TYPED = "espresso";
    private String PASS_TO_BE_TYPED = "123456789";

    @Rule
    public ActivityScenarioRule<LoginActivity> activityScenarioRule = new ActivityScenarioRule<>(LoginActivity.class);

    @Test
    public void register_accoount_on_spotify() {
        onView(withId(R.id.registerText)).perform(click());
        onView(withId(R.id.registerActivity)).check(matches(isDisplayed()));
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etrepeatpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etusername)).perform(typeText(USERNAME_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.signupButton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));
    }

    @Test
    public void login_accoount_on_spotify() {
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.loginbutton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));
    }
}
