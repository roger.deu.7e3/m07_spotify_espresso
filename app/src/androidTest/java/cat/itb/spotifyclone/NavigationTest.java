package cat.itb.spotifyclone;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class NavigationTest {

    private String USER_TO_BE_TYPED = "espressoprova12@gmail.com";
    private String PASS_TO_BE_TYPED = "123456789";

    @Rule
    public ActivityScenarioRule<LoginActivity> activityScenarioRule = new ActivityScenarioRule<>(LoginActivity.class);

    @Test
    public void go_to_settings_fragment() {
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.loginbutton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));

        onView(withId(R.id.settingsButton)).perform(click());
        onView(withId(R.id.settingsFragmenttest)).check(matches(isDisplayed()));
    }

    @Test
    public void enter_on_search_and_show_fragment_search() {
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.loginbutton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));

        onView(withId(R.id.navigation_dashboard)).perform(click());
        onView(withId(R.id.searchFragment)).check(matches(isDisplayed()));
    }

    @Test
    public void enter_on_library_and_show_fragment_library() {
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.loginbutton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));

        onView(withId(R.id.basePageLibrary2)).perform(click());
        onView(withId(R.id.basePageTabs)).check(matches(isDisplayed()));
    }

    @Test
    public void test_recyclerview() {
        onView(withId(R.id.etloginUser)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.etpass)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.loginbutton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.container)).check(matches(isDisplayed()));

        onView(withId(R.id.recyclerView1)).perform(actionOnItemAtPosition(2,click()));
        onView(withId(R.id.fragmentAlbumList)).check(matches(isDisplayed()));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.titleAlbum)).check(matches(withText("EL ÚLTIMO TOUR DEL MUNDO")));
    }
}
